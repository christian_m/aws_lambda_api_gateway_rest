variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "lambda_name" {
  description = "name of the lambda function"
  type        = string
}

variable "lambda_function_invoke_arn" {
  description = "invoke arn of the lambda function"
  type        = string
}

variable "domain_name" {
  description = "name of the domain where the api-gateway is available"
  type        = string
}

variable "zone_id" {
  description = "id of the domain zone"
  type        = string
}

locals {
  api_name = "${var.lambda_name}-api"
}