resource "aws_api_gateway_account" "default" {
  cloudwatch_role_arn = aws_iam_role.cloudwatch.arn
}

resource "aws_api_gateway_rest_api" "default" {
  name        = local.api_name
  description = "${var.lambda_name} Lambda Function API"

  tags = {
    env = var.environment
  }
}

resource "aws_api_gateway_resource" "proxy" {
  rest_api_id = aws_api_gateway_rest_api.default.id
  parent_id   = aws_api_gateway_rest_api.default.root_resource_id
  path_part   = "{proxy+}"
}

resource "aws_api_gateway_method" "proxy" {
  rest_api_id   = aws_api_gateway_rest_api.default.id
  resource_id   = aws_api_gateway_resource.proxy.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "default" {
  rest_api_id = aws_api_gateway_rest_api.default.id
  resource_id = aws_api_gateway_method.proxy.resource_id
  http_method = aws_api_gateway_method.proxy.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = var.lambda_function_invoke_arn
}

resource "aws_api_gateway_deployment" "default" {
  depends_on = [aws_api_gateway_integration.default]

  rest_api_id = aws_api_gateway_rest_api.default.id
}

resource "aws_api_gateway_stage" "default" {
  deployment_id = aws_api_gateway_deployment.default.id
  rest_api_id   = aws_api_gateway_rest_api.default.id
  stage_name    = local.api_name

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_access_log.arn
    format          = jsonencode({
      "requestId" : "$context.requestId",
      "extendedRequestId" : "$context.extendedRequestId",
      "ip" : "$context.identity.sourceIp",
      "caller" : "$context.identity.caller",
      "user" : "$context.identity.user",
      "requestTime" : "$context.requestTime",
      "httpMethod" : "$context.httpMethod",
      "resourcePath" : "$context.resourcePath",
      "status" : "$context.status",
      "protocol" : "$context.protocol",
      "responseLength" : "$context.responseLength"
    })
  }

  tags = {
    env = var.environment
  }
}

resource "aws_api_gateway_method_settings" "default" {
  rest_api_id = aws_api_gateway_rest_api.default.id
  stage_name  = aws_api_gateway_stage.default.stage_name
  method_path = "*/*"

  settings {
    metrics_enabled    = true
    data_trace_enabled = true
    logging_level      = "INFO"

    # Limit the rate of calls to prevent abuse and unwanted charges
    throttling_rate_limit  = 100
    throttling_burst_limit = 50
  }
}

resource "aws_lambda_permission" "allow_lambda_execution" {
  statement_id  = "AllowAPIGatewayInvoke"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_name
  principal     = "apigateway.amazonaws.com"

  # The "/*/*" portion grants access from any method on any resource
  # within the API Gateway REST API.
  source_arn = "${aws_api_gateway_rest_api.default.execution_arn}/*/*"
}

module "domain_certificate" {
  source      = "git::git@bitbucket.org:christian_m/aws_domain_certificate.git?ref=v1.0"
  environment = var.environment
  domain_name = var.domain_name
  zone_id     = var.zone_id
}

resource "aws_api_gateway_domain_name" "default" {
  domain_name              = var.domain_name
  regional_certificate_arn = module.domain_certificate.arn

  endpoint_configuration {
    types = ["REGIONAL"]
  }

  tags = {
    env = var.environment
  }
}

module "gateway_domain_alias" {
  source          = "git::git@bitbucket.org:christian_m/aws_route53_resource_alias.git?ref=v1.0"
  environment     = var.environment
  allow_overwrite = true
  zone_id         = var.zone_id
  record          = {
    name = var.domain_name
    type = "A"
  }
  aliases = [
    {
      evaluate_target_health = false
      name                   = aws_api_gateway_domain_name.default.regional_domain_name
      zone_id                = aws_api_gateway_domain_name.default.regional_zone_id
    },
  ]
}

resource "aws_api_gateway_base_path_mapping" "default" {
  api_id      = aws_api_gateway_rest_api.default.id
  stage_name  = aws_api_gateway_stage.default.stage_name
  domain_name = aws_api_gateway_domain_name.default.domain_name
}