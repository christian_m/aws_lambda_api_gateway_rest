resource "aws_cloudwatch_log_group" "api_access_log" {
  name              = "/aws/apigateway/${local.api_name}"
  retention_in_days = 30
}

resource "aws_cloudwatch_log_group" "api_logs" {
  name              = "API-Gateway-Execution-Logs_${aws_api_gateway_deployment.default.rest_api_id}/${local.api_name}"
  retention_in_days = 30
}

resource "aws_iam_role" "cloudwatch" {
  name = "api_gateway_cloudwatch_${local.api_name}"

  assume_role_policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Sid" : "",
        "Effect" : "Allow",
        "Principal" : {
          "Service" : "apigateway.amazonaws.com"
        },
        "Action" : "sts:AssumeRole"
      }
    ]
  })
}

resource "aws_iam_role_policy" "allow_cloudwatch_logging" {
  name = "api_gateway_rest_cloudwatch_${local.api_name}"
  role = aws_iam_role.cloudwatch.id

  policy = jsonencode({
    "Version" : "2012-10-17",
    "Statement" : [
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogGroup",
          "logs:CreateLogStream",
          "logs:DescribeLogGroups",
          "logs:DescribeLogStreams",
          "logs:PutLogEvents",
          "logs:GetLogEvents",
          "logs:FilterLogEvents"
        ],
        Resource : "arn:aws:logs:*:*:*"
      }
    ]
  })
}