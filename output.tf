output "api_url" {
  value = "${aws_api_gateway_deployment.default.invoke_url}${aws_api_gateway_stage.default.stage_name}"
}

output "access_log_group_name" {
  value = aws_cloudwatch_log_group.api_access_log.name
}